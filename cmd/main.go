package main

import (
	"citix-test-task/internal/app"
	"citix-test-task/internal/cfg"
	"go.uber.org/zap"
)

func main() {
	log, _ := zap.NewProduction()
	zap.ReplaceGlobals(log)

	appcfg, err := cfg.NewAppConfig()
	if err != nil {
		zap.S().Fatalf("Can't initialize configuration: %s", err)
	}

	app.NewApplication(appcfg).Start()
}
