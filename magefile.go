//go:build mage

package main

import (
	"github.com/magefile/mage/sh"
	"os"
	"path/filepath"
)

const (
	main_file_location = "cmd/main.go"
	binary_name        = "catalog"
)

func Build() error {
	// Create build directory, if it's not exists
	if err := os.MkdirAll(build_artefacts_path, 0755); err != nil {
		return err
	}

	// build binary
	sh.RunV("go", "build", "-o", filepath.Join(build_artefacts_path, binary_name), main_file_location)
	return nil
}
