FROM golang:latest AS build

# Install mage
RUN git clone https://github.com/magefile/mage && \
    cd mage && \
    go run bootstrap.go

RUN mkdir -p /build && \
    cd /build

WORKDIR /build
COPY . .

RUN mage build

FROM ubuntu:20.04

RUN mkdir -p /opt/catalog

WORKDIR /opt/catalog
COPY --from=build /build/build/catalog /opt/catalog/catalog
COPY --from=build /build/config.toml /opt/catalog/config.toml

EXPOSE 9090
EXPOSE 9091
EXPOSE 9092

ENTRYPOINT ["/opt/catalog/catalog"]