package cfg

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"os"
)

type AppConfig struct {
	DB   DBConfig       `mapstructure:"database"`
	HTTP HttpRestConfig `mapstructure:"http_server"`
	WS   WSConfig       `mapstructure:"websocket"`
	GRPC GRPCConfig     `mapstructure:"grpc_config"`
}

type DBConfig struct {
	Host         string `mapstructure:"host"`
	Username     string `mapstructure:"username"`
	Password     string `mapstructure:"password"`
	DatabaseName string `mapstructure:"dbname"`
	DatabasePort uint16 `mapstructure:"dbport"`
}

type HttpRestConfig struct {
	Use        bool   `mapstructure:"use"`
	ListenAddr string `mapstructure:"listen_addr"`
	ListenPort uint16 `mapstructure:"listen_port"`
}

type WSConfig struct {
	Use        bool   `mapstructure:"use"`
	ListenAddr string `mapstructure:"listen_addr"`
	ListenPort uint16 `mapstructure:"listen_port"`
}

type GRPCConfig struct {
	Use        bool   `mapstructure:"use"`
	ListenAddr string `mapstructure:"listen_addr"`
	ListenPort uint16 `mapstructure:"listen_port"`
}

func NewAppConfig() (*AppConfig, error) {
	config := &AppConfig{}

	configFileName, defined := os.LookupEnv("CATALOG_CONFFILE")
	if !defined {
		configFileName = "config.toml"
	}

	viperConfig := viper.New()
	viperConfig.SetConfigFile(configFileName)
	viperConfig.SetConfigType("toml")
	if err := viperConfig.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "config.NewAppConfig")
	}

	if err := viperConfig.Unmarshal(config); err != nil {
		return nil, errors.Wrap(err, "config.NewAppConfig")
	}

	return config, nil
}
