package app

import (
	"citix-test-task/internal/cfg"
	"citix-test-task/internal/db"
	"citix-test-task/internal/server"
	"citix-test-task/internal/server/grpc"
	"citix-test-task/internal/server/rest"
	"citix-test-task/internal/server/ws"
	"citix-test-task/internal/service"
	"context"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"os/signal"
	"syscall"
)

type Application struct {
	appcfg *cfg.AppConfig
}

func NewApplication(appcfg *cfg.AppConfig) *Application {
	return &Application{
		appcfg: appcfg,
	}
}

func (a *Application) Start() {
	appCtx, _ := signal.NotifyContext(
		context.Background(),
		syscall.SIGTERM,
		syscall.SIGQUIT,
		syscall.SIGINT,
	)
	g, appCtx := errgroup.WithContext(appCtx)

	catalogDB, err := db.InitializeDB(a.appcfg)
	if err != nil {
		zap.S().With("err", err).Fatal("Unable to initialize database")
	}

	catalogService := service.NewCatalogService(a.appcfg, catalogDB)

	servers := []server.Server{
		ws.NewWebsocketServer(appCtx, a.appcfg, catalogService),
		rest.NewRestHttpServer(appCtx, a.appcfg, catalogService),
		grpc.NewGrpcServer(appCtx, a.appcfg, catalogService),
	}

	for _, srv := range servers {
		g.Go(srv.ListenAndServe)
	}

	_ = g.Wait()
}
