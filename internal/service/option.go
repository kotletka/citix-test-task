package service

type GetOpts interface {
	getOptsData() getOptsData
}

type getOptsData struct {
	Pagination     bool
	PaginationFrom int
	PaginationSize int
}

type PaginationOption struct {
	from int
	size int
}

func NewPaginationOption(from int, size int) GetOpts {
	return &PaginationOption{
		from: from,
		size: size,
	}
}

func (o *PaginationOption) getOptsData() getOptsData {
	return getOptsData{
		Pagination:     true,
		PaginationSize: o.size,
		PaginationFrom: o.from,
	}
}
