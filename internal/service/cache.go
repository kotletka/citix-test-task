package service

import (
	"errors"
	"sync"
	"time"
)

const (
	cacheExpirationDuration = 3 * time.Minute
)

var (
	ErrRecordNotExistOrExpired = errors.New("record doesn't exist or expired")
)

type cachedRecord struct {
	record    *CatalogRecord
	timestamp time.Time
}

type CacheService struct {
	sync.RWMutex
	cachedRecords          map[uint]cachedRecord
	fullRecordListSupplied bool
}

func NewCacheService() *CacheService {
	return &CacheService{
		cachedRecords: make(map[uint]cachedRecord),
	}
}

func (c *CacheService) IsAllRecordsHadBeenSupplied() bool {
	return c.fullRecordListSupplied
}

func (c *CacheService) RecordExpiredOrNotExist(recordId uint) bool {
	c.RLock()
	defer c.RUnlock()

	cacheRecord, has := c.cachedRecords[recordId]
	return !has || time.Now().Sub(cacheRecord.timestamp) > cacheExpirationDuration
}

func (c *CacheService) InsertRecord(record *CatalogRecord) {
	if !c.RecordExpiredOrNotExist(record.ID) {
		return
	}

	c.Lock()
	defer c.Unlock()
	c.cachedRecords[record.ID] = cachedRecord{
		record:    record,
		timestamp: time.Now(),
	}
}

func (c *CacheService) UpdateRecord(record *CatalogRecord) {
	c.Lock()
	defer c.Unlock()
	c.cachedRecords[record.ID] = cachedRecord{
		record:    record,
		timestamp: time.Now(),
	}
}

func (c *CacheService) DeleteRecord(recordId uint) {
	c.Lock()
	defer c.Unlock()

	delete(c.cachedRecords, recordId)
}

func (c *CacheService) GetRecordsWithoutCheck() []*CatalogRecord {
	records := make([]*CatalogRecord, 0, len(c.cachedRecords))

	c.RLock()
	for _, cacheRecord := range c.cachedRecords {
		records = append(records, cacheRecord.record)
	}
	c.RUnlock()

	return records
}

func (c *CacheService) GetRecord(recordId uint) (*CatalogRecord, error) {
	if c.RecordExpiredOrNotExist(recordId) {
		return nil, ErrRecordNotExistOrExpired
	}

	c.RLock()
	defer c.RUnlock()
	return c.cachedRecords[recordId].record, nil
}

func (c *CacheService) InsertAllRecords(records []*CatalogRecord) {
	for _, record := range records {
		c.InsertRecord(record)
	}

	c.fullRecordListSupplied = true
}
