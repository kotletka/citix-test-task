package service

import (
	"citix-test-task/internal/cfg"
	"citix-test-task/internal/db/model"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"sort"
)

var (
	ErrRecordNotExists = errors.New("record doesn't exist")
)

type CatalogRecord struct {
	ID                 uint
	ProductName        string
	ProductDescription string
	ProductPrice       uint64
	Availability       CatalogRecordAvailability
}

type CatalogService struct {
	config       *cfg.AppConfig
	catalogDB    *gorm.DB
	cacheService *CacheService
}

func NewCatalogService(config *cfg.AppConfig, catalogDB *gorm.DB) *CatalogService {
	return &CatalogService{
		config:       config,
		catalogDB:    catalogDB,
		cacheService: NewCacheService(),
	}
}

func (s *CatalogService) CreateCatalogRecord(record CatalogRecord) (uint, error) {
	newRecord := &model.CatalogRecord{
		ProductName:        record.ProductName,
		ProductDescription: record.ProductDescription,
		ProductPrice:       record.ProductPrice,
		Availability:       uint8(record.Availability),
	}

	if err := s.catalogDB.Create(newRecord).Error; err != nil {
		return 0, errors.Wrap(err, "service.CreateCatalogService")
	}

	record.ID = newRecord.ID
	s.cacheService.InsertRecord(&record)
	return newRecord.ID, nil
}

func (s *CatalogService) DeleteCatalogRecord(recordId uint) error {
	res := s.catalogDB.Delete(&model.CatalogRecord{ID: recordId})

	if res.Error != nil {
		return errors.Wrap(res.Error, "service.DeleteCatalogRecord")
	}

	if res.RowsAffected == 0 {
		return ErrRecordNotExists
	}

	s.cacheService.DeleteRecord(recordId)
	return nil
}

func (s *CatalogService) UpdateCatalogRecord(record CatalogRecord) error {
	modelRecord := &model.CatalogRecord{ID: record.ID}
	res := s.catalogDB.First(&modelRecord)

	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return ErrRecordNotExists
	} else if res.Error != nil {
		return errors.Wrap(res.Error, "service.UpdateCatalogRecord")
	}

	s.mergeCatalogIntoModel(modelRecord, record)
	s.catalogDB.Save(modelRecord)
	s.cacheService.UpdateRecord(&record)
	return nil
}

func (s *CatalogService) GetCatalogRecord(recordId uint) (*CatalogRecord, error) {
	cacheRecord, _ := s.cacheService.GetRecord(recordId)
	if cacheRecord != nil {
		return cacheRecord, nil
	}

	modelRecord := &model.CatalogRecord{ID: recordId}
	res := s.catalogDB.First(modelRecord)

	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return nil, ErrRecordNotExists
	} else if res.Error != nil {
		return nil, errors.Wrap(res.Error, "service.GetCatalogRecord")
	}

	extractedRecord := &CatalogRecord{
		ID:                 recordId,
		ProductName:        modelRecord.ProductName,
		ProductDescription: modelRecord.ProductDescription,
		ProductPrice:       modelRecord.ProductPrice,
		Availability:       CatalogRecordAvailability(modelRecord.Availability),
	}

	s.cacheService.InsertRecord(extractedRecord)
	return extractedRecord, nil
}

func (s *CatalogService) GetCatalogRecordList(opts ...GetOpts) ([]*CatalogRecord, error) {
	var (
		usePagination  bool
		paginationFrom int
		paginationSize int
		err            error
	)

	for _, getOpt := range opts {
		getOptData := getOpt.getOptsData()
		if getOptData.Pagination {
			usePagination = true
		}

		if getOptData.PaginationFrom != 0 {
			paginationFrom = getOptData.PaginationFrom
		}

		if getOptData.PaginationSize != 0 {
			paginationSize = getOptData.PaginationSize
		}
	}

	var allRecords []*CatalogRecord
	if !s.cacheService.IsAllRecordsHadBeenSupplied() {
		allRecords, err = s.extractAllRecordsAndSaveToCache()
	} else {
		// If there is a record, that expired, we need to extract records again
		allRecords = s.cacheService.GetRecordsWithoutCheck()
		for _, record := range allRecords {
			if s.cacheService.RecordExpiredOrNotExist(record.ID) {
				allRecords, err = s.extractAllRecordsAndSaveToCache()
				break
			}
		}
	}

	if err != nil {
		return nil, errors.Wrap(err, "service.GetCatalogRecordList")
	}

	sort.Slice(allRecords, func(i, j int) bool {
		return allRecords[i].ID < allRecords[j].ID
	})

	if usePagination {
		if paginationFrom > len(allRecords) {
			paginationFrom = 0
		}

		paginationTo := paginationFrom + paginationSize
		if paginationTo > len(allRecords) {
			paginationTo = len(allRecords)
		}

		allRecords = allRecords[paginationFrom:paginationTo]
	}

	return allRecords, nil
}

func (s *CatalogService) extractAllRecordsAndSaveToCache() ([]*CatalogRecord, error) {
	allRecords := make([]model.CatalogRecord, 0)
	res := s.catalogDB.Find(&allRecords)

	if res.Error != nil {
		return nil, res.Error
	}

	catalogRecords := make([]*CatalogRecord, 0, len(allRecords))
	for _, modelRecord := range allRecords {
		catalogRecords = append(catalogRecords, &CatalogRecord{
			ID:                 modelRecord.ID,
			ProductName:        modelRecord.ProductName,
			ProductDescription: modelRecord.ProductDescription,
			ProductPrice:       modelRecord.ProductPrice,
			Availability:       CatalogRecordAvailability(modelRecord.Availability),
		})
	}

	s.cacheService.InsertAllRecords(catalogRecords)
	return catalogRecords, nil
}

func (s *CatalogService) mergeCatalogIntoModel(m *model.CatalogRecord, record CatalogRecord) {
	if record.ProductName != "" {
		m.ProductName = record.ProductName
	}

	if record.ProductDescription != "" {
		m.ProductDescription = record.ProductDescription
	}

	if record.ProductPrice != 0 {
		m.ProductPrice = record.ProductPrice
	}

	if record.Availability != 0 {
		m.Availability = uint8(record.Availability)
	}
}
