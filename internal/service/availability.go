package service

type CatalogRecordAvailability int

func (c CatalogRecordAvailability) String() string {
	switch c {
	case Unavailable:
		return "unavailable"
	case FewAvailable:
		return "few_available"
	case ModerateAvailable:
		return "moderate_available"
	case FullyAvailable:
		return "fully_available"
	default:
		return ""
	}
}

func NewCatalogRecordAvailability(str string) CatalogRecordAvailability {
	switch str {
	case "unavailable":
		return Unavailable
	case "few_available":
		return FewAvailable
	case "moderate_available":
		return ModerateAvailable
	case "fully_available":
		return FullyAvailable
	default:
		return 0
	}
}

const (
	Unavailable CatalogRecordAvailability = iota + 1
	FewAvailable
	ModerateAvailable
	FullyAvailable
)
