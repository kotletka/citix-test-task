package ws

import (
	"citix-test-task/internal/cfg"
	"citix-test-task/internal/server/ws/wshandler"
	"citix-test-task/internal/service"
	"context"
	"fmt"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
	"net/http"
)

var (
	websocketUpgrader = websocket.Upgrader{
		// For testing purposes
		CheckOrigin: func(_ *http.Request) bool {
			return true
		},
	}
)

type WebsocketServer struct {
	appCtx    context.Context
	config    *cfg.AppConfig
	catalog   *service.CatalogService
	wsHandler *wshandler.WSHandler
}

func NewWebsocketServer(appCtx context.Context, config *cfg.AppConfig, catalog *service.CatalogService) *WebsocketServer {
	return &WebsocketServer{
		appCtx:    appCtx,
		config:    config,
		catalog:   catalog,
		wsHandler: wshandler.NewWSHandler(catalog),
	}
}

func (s *WebsocketServer) ListenAndServe() error {
	router := http.NewServeMux()
	router.HandleFunc("/ws", s.upgradeToWebsocketConnection)

	httpServer := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", s.config.WS.ListenAddr, s.config.WS.ListenPort),
		Handler: router,
	}

	var ctxClosed bool
	errChan := make(chan error)
	defer close(errChan)

	go func() {
		err := httpServer.ListenAndServe()
		if !ctxClosed {
			errChan <- err
		}
	}()

	select {
	case <-s.appCtx.Done():
		ctxClosed = true
		_ = httpServer.Shutdown(context.Background())
		return nil
	case err := <-errChan:
		zap.S().With("err", err).Error("Error occured when serving ws server")
		return err
	}
}

func (s *WebsocketServer) upgradeToWebsocketConnection(w http.ResponseWriter, r *http.Request) {
	c, err := websocketUpgrader.Upgrade(w, r, nil)
	if err != nil {
		zap.S().With("err", err).Error("Can't upgrade websocket connection")
		return
	}

	s.wsHandler.HandleConnection(c)
}
