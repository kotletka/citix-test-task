package wshandler

var (
	badRequestResponse = map[string]interface{}{
		"error": "Bad request",
	}

	internalErrorResponse = map[string]interface{}{
		"error": "Internal server error",
	}

	recordNotFoundResponse = map[string]interface{}{
		"error": "Record not found",
	}

	okResponse = map[string]interface{}{
		"result": "ok",
	}

	unknownCommandResponse = map[string]interface{}{
		"error": "Unknown command",
	}
)

type websocketCommand string

const (
	create  websocketCommand = "create"
	upgrade websocketCommand = "upgrade"
	del     websocketCommand = "delete"
	get     websocketCommand = "get"
	list    websocketCommand = "list"
)

type baseWebsocketRequest struct {
	Command websocketCommand `json:"command,omitempty"`

	ID                 uint   `json:"id,omitempty"`
	ProductName        string `json:"productName,omitempty"`
	ProductDescription string `json:"productDescription,omitempty"`
	ProductPrice       uint64 `json:"productPrice,omitempty"`
	Availability       string `json:"availability,omitempty"`

	PaginationParams *paginationParameters `json:"pagination,omitempty"`
}

type catalogRecordModel struct {
	ID                 uint   `json:"id,omitempty"`
	ProductName        string `json:"productName,omitempty"`
	ProductDescription string `json:"productDescription,omitempty"`
	ProductPrice       uint64 `json:"productPrice,omitempty"`
	Availability       string `json:"availability,omitempty"`
}

type paginationParameters struct {
	From int `json:"from"`
	Size int `json:"size"`
}
