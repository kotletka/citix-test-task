package wshandler

import (
	"citix-test-task/internal/service"
	"errors"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

type WSHandler struct {
	catalog *service.CatalogService
}

func NewWSHandler(catalog *service.CatalogService) *WSHandler {
	return &WSHandler{
		catalog: catalog,
	}
}

func (s *WSHandler) HandleConnection(conn *websocket.Conn) {
	for {
		request := &baseWebsocketRequest{}
		if err := conn.ReadJSON(request); err != nil {
			zap.S().With("err", err).Error("wshandler.HandleConnection: Can't read data from client")
			return
		}

		s.handleCommand(conn, request)
	}
}

func (s *WSHandler) handleCreateCommand(conn *websocket.Conn, request *baseWebsocketRequest) {
	recordId, err := s.catalog.CreateCatalogRecord(service.CatalogRecord{
		ProductName:        request.ProductName,
		ProductDescription: request.ProductDescription,
		ProductPrice:       request.ProductPrice,
		Availability:       service.NewCatalogRecordAvailability(request.Availability),
	})

	if err != nil {
		zap.S().With("err", err).Error("wshandler.handleCreateCommand: Can't create new record")
		_ = conn.WriteJSON(internalErrorResponse)
		return
	}

	_ = conn.WriteJSON(&catalogRecordModel{
		ID: recordId,
	})
}

func (s *WSHandler) handleDeleteCommand(conn *websocket.Conn, request *baseWebsocketRequest) {
	err := s.catalog.DeleteCatalogRecord(request.ID)

	if errors.Is(err, service.ErrRecordNotExists) {
		_ = conn.WriteJSON(recordNotFoundResponse)
		return
	} else if err != nil {
		zap.S().With("err", err).Error("wshandler.handleDeleteCommand: Can't delete record")
		_ = conn.WriteJSON(internalErrorResponse)
		return
	}

	_ = conn.WriteJSON(okResponse)
}

func (s *WSHandler) handleUpgradeCommand(conn *websocket.Conn, request *baseWebsocketRequest) {
	err := s.catalog.UpdateCatalogRecord(service.CatalogRecord{
		ID:                 request.ID,
		ProductName:        request.ProductName,
		ProductDescription: request.ProductDescription,
		ProductPrice:       request.ProductPrice,
		Availability:       service.NewCatalogRecordAvailability(request.Availability),
	})

	if errors.Is(err, service.ErrRecordNotExists) {
		_ = conn.WriteJSON(recordNotFoundResponse)
		return
	} else if err != nil {
		zap.S().With("err", err).Error("wshandler.handleUgradeCommand: Can't upgrade record")
		_ = conn.WriteJSON(internalErrorResponse)
		return
	}

	_ = conn.WriteJSON(okResponse)
}

func (s *WSHandler) handleGetCommand(conn *websocket.Conn, request *baseWebsocketRequest) {
	record, err := s.catalog.GetCatalogRecord(request.ID)

	if errors.Is(err, service.ErrRecordNotExists) {
		_ = conn.WriteJSON(recordNotFoundResponse)
		return
	} else if err != nil {
		zap.S().With("err", err).Error("wshandler.handleGetCommand: Can't get record")
		_ = conn.WriteJSON(internalErrorResponse)
		return
	}

	_ = conn.WriteJSON(&catalogRecordModel{
		ProductName:        record.ProductName,
		ProductDescription: record.ProductDescription,
		ProductPrice:       request.ProductPrice,
		Availability:       record.Availability.String(),
	})
}

func (s *WSHandler) handleListCommand(conn *websocket.Conn, request *baseWebsocketRequest) {
	getOpts := []service.GetOpts{}

	if request.PaginationParams != nil {
		getOpts = append(getOpts, service.NewPaginationOption(
			request.PaginationParams.From,
			request.PaginationParams.Size,
		))
	}

	recordList, err := s.catalog.GetCatalogRecordList(getOpts...)

	if err != nil {
		zap.S().With("err", err).Error("wshandler.handleListCommand: Can't list records")
		_ = conn.WriteJSON(internalErrorResponse)
		return
	}

	responseList := make([]catalogRecordModel, 0, len(recordList))
	for _, record := range recordList {
		responseList = append(responseList, catalogRecordModel{
			ID:                 record.ID,
			ProductName:        record.ProductName,
			ProductDescription: record.ProductDescription,
			ProductPrice:       record.ProductPrice,
			Availability:       record.Availability.String(),
		})
	}

	_ = conn.WriteJSON(responseList)
}

func (s *WSHandler) handleCommand(conn *websocket.Conn, request *baseWebsocketRequest) {
	switch request.Command {
	case create:
		s.handleCommand(conn, request)
	case del:
		s.handleDeleteCommand(conn, request)
	case upgrade:
		s.handleUpgradeCommand(conn, request)
	case get:
		s.handleGetCommand(conn, request)
	case list:
		s.handleListCommand(conn, request)
	default:
		_ = conn.WriteJSON(unknownCommandResponse)
	}
}
