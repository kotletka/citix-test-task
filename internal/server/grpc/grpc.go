package grpc

import (
	"citix-test-task/internal/cfg"
	"citix-test-task/internal/server/grpc/proto"
	"citix-test-task/internal/service"
	"context"
	"fmt"
	"github.com/icza/gog"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"net"
)

type GrpcServer struct {
	proto.UnimplementedCatalogServer
	appCtx  context.Context
	config  *cfg.AppConfig
	catalog *service.CatalogService
}

func NewGrpcServer(appCtx context.Context, config *cfg.AppConfig, catalog *service.CatalogService) *GrpcServer {
	return &GrpcServer{
		appCtx:  appCtx,
		config:  config,
		catalog: catalog,
	}
}

func (s *GrpcServer) ListenAndServe() error {
	grpcSocket, _ := net.Listen("tcp",
		fmt.Sprintf("%s:%d", s.config.GRPC.ListenAddr, s.config.GRPC.ListenPort),
	)

	grpcServer := grpc.NewServer()
	proto.RegisterCatalogServer(grpcServer, s)

	var ctxClosed bool
	errChan := make(chan error)
	defer close(errChan)

	go func() {
		err := grpcServer.Serve(grpcSocket)
		if !ctxClosed {
			errChan <- err
		}
	}()

	select {
	case <-s.appCtx.Done():
		ctxClosed = true
		grpcServer.Stop()
		return nil
	case err := <-errChan:
		zap.S().With("err", err).Error("Error when serving GRPC server occured")
		return err
	}
}

func (s *GrpcServer) CreateCatalogRecord(ctx context.Context, message *proto.CatalogRecordMessage) (*proto.IdCatalogRecordMessage, error) {
	recordId, err := s.catalog.CreateCatalogRecord(service.CatalogRecord{
		ProductName:        message.GetProductName(),
		ProductDescription: message.GetProductDescription(),
		ProductPrice:       message.GetProductPrice(),
		Availability:       service.CatalogRecordAvailability(message.GetAvailability()),
	})

	if err != nil {
		zap.S().With("err", err).Error("Can't create new record")
		return nil, err
	}

	return &proto.IdCatalogRecordMessage{
		Id: gog.Ptr(uint64(recordId)),
	}, nil
}

func (s *GrpcServer) DeleteCatalogRecordMessage(ctx context.Context, message *proto.IdCatalogRecordMessage) (*emptypb.Empty, error) {
	err := s.catalog.DeleteCatalogRecord(uint(message.GetId()))

	if err != nil {
		zap.S().With("err", err).Error("Can't delete record")
		return nil, err
	}

	return nil, nil
}

func (s *GrpcServer) UpdateCatalogRecordMessage(ctx context.Context, message *proto.IdCatalogRecordMessage) (*emptypb.Empty, error) {
	catalogRecord := message.GetRecord()
	if catalogRecord == nil {
		return nil, nil
	}

	err := s.catalog.UpdateCatalogRecord(service.CatalogRecord{
		ID:                 uint(message.GetId()),
		ProductName:        catalogRecord.GetProductName(),
		ProductDescription: catalogRecord.GetProductDescription(),
		ProductPrice:       catalogRecord.GetProductPrice(),
		Availability:       service.CatalogRecordAvailability(catalogRecord.GetAvailability()),
	})

	if err != nil {
		zap.S().With("err", err).Error("Can't update record")
		return nil, err
	}

	return nil, nil
}

func (s *GrpcServer) GetCatalogRecordMessage(ctx context.Context, message *proto.IdCatalogRecordMessage) (*proto.CatalogRecordMessage, error) {
	record, err := s.catalog.GetCatalogRecord(uint(message.GetId()))
	if err != nil {
		zap.S().With("err", err).Error("Can't get record from catalog")
		return nil, err
	}

	return &proto.CatalogRecordMessage{
		ProductName:        &record.ProductName,
		ProductDescription: &record.ProductDescription,
		ProductPrice:       &record.ProductPrice,
		Availability:       gog.Ptr(uint32(record.Availability)),
	}, nil
}

func (s *GrpcServer) GetCatalogRecordMessageList(ctx context.Context, _ *emptypb.Empty) (*proto.IdCatalogRecordListMessage, error) {
	records, err := s.catalog.GetCatalogRecordList()
	if err != nil {
		zap.S().With("err", err).Error("Can't get catalog list")
		return nil, err
	}

	protoRecordList := make([]*proto.IdCatalogRecordMessage, 0, len(records))
	for _, record := range records {
		protoRecordList = append(protoRecordList, &proto.IdCatalogRecordMessage{
			Id: gog.Ptr(uint64(record.ID)),
			Record: &proto.CatalogRecordMessage{
				ProductName:        &record.ProductName,
				ProductDescription: &record.ProductDescription,
				ProductPrice:       &record.ProductPrice,
				Availability:       gog.Ptr(uint32(record.Availability)),
			},
		})
	}

	return &proto.IdCatalogRecordListMessage{
		List: protoRecordList,
	}, nil
}

func (s *GrpcServer) GetCatalogRecordMessageStream(_ *emptypb.Empty, server proto.Catalog_GetCatalogRecordMessageStreamServer) error {
	records, err := s.catalog.GetCatalogRecordList()
	if err != nil {
		zap.S().With("err", err).Error("Can't get catalog list")
		return err
	}

	for _, record := range records {
		err := server.Send(&proto.IdCatalogRecordMessage{
			Id: gog.Ptr(uint64(record.ID)),
			Record: &proto.CatalogRecordMessage{
				ProductName:        &record.ProductName,
				ProductDescription: &record.ProductDescription,
				ProductPrice:       &record.ProductPrice,
				Availability:       gog.Ptr(uint32(record.Availability)),
			},
		})

		if err != nil {
			zap.S().With("err", err).Error("Failed to send record to proto client within stream")
			return err
		}
	}

	return nil
}
