package rest

import (
	"citix-test-task/internal/service"
	"errors"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

func (s *RestHttpServer) HandleCreateCatalogRecord(ctx *gin.Context) {
	request := &catalogRecordModel{}
	if err := ctx.BindJSON(request); err != nil {
		zap.S().With("err", err).Error("rest.HandleCreateCatalogRecord: Error parsing request body")
		return
	}

	recordId, err := s.catalog.CreateCatalogRecord(service.CatalogRecord{
		ProductName:        request.ProductName,
		ProductDescription: request.ProductDescription,
		ProductPrice:       request.ProductPrice,
		Availability:       service.NewCatalogRecordAvailability(request.Availability),
	})

	if err != nil {
		zap.S().With("err", err).Error("rest.HandleCreateCatalogRecord: Error creating record")
		ctx.JSON(http.StatusInternalServerError, internalErrorResponse)
		return
	}

	ctx.JSON(http.StatusOK, &catalogRecordModel{
		ID: recordId,
	})
}

func (s *RestHttpServer) HandleDeleteCatalogRecord(ctx *gin.Context) {
	recordIdString := ctx.Param("id")

	recordId, err := strconv.ParseInt(recordIdString, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, badRequestResponse)
		return
	}

	err = s.catalog.DeleteCatalogRecord(uint(recordId))
	if errors.Is(err, service.ErrRecordNotExists) {
		ctx.JSON(http.StatusNotFound, recordNotFoundResponse)
		return
	} else if err != nil {
		zap.S().With("err", err).Error("rest.HandleDeleteCatalogRecord: Error deleting record")
		ctx.JSON(http.StatusInternalServerError, internalErrorResponse)
		return
	}

	ctx.String(http.StatusOK, "{}")
}

func (s *RestHttpServer) HandleUpdateCatalogRecord(ctx *gin.Context) {
	recordIdString := ctx.Param("id")

	recordId, err := strconv.ParseInt(recordIdString, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, badRequestResponse)
		return
	}

	request := &catalogRecordModel{}
	if err := ctx.BindJSON(request); err != nil {
		zap.S().With("err", err).Error("rest.HandleUpdateCatalogRecord: Error parsing request body")
		return
	}

	err = s.catalog.UpdateCatalogRecord(service.CatalogRecord{
		ID:                 uint(recordId),
		ProductName:        request.ProductName,
		ProductDescription: request.ProductDescription,
		ProductPrice:       request.ProductPrice,
		Availability:       service.NewCatalogRecordAvailability(request.Availability),
	})

	if errors.Is(err, service.ErrRecordNotExists) {
		ctx.JSON(http.StatusNotFound, recordNotFoundResponse)
		return
	} else if err != nil {
		zap.S().With("err", err).Error("rest.HandleUpdateCatalogRecord: Error updating record")
		ctx.JSON(http.StatusInternalServerError, internalErrorResponse)
		return
	}

	ctx.String(http.StatusOK, "{}")
}

func (s *RestHttpServer) HandleListCatalogRecord(ctx *gin.Context) {
	getOpts := []service.GetOpts{}

	if ctx.FullPath() == "/catalog/list/page" {
		request := &listPaginationParameters{}
		if err := ctx.BindJSON(request); err != nil {
			zap.S().With("err", err).Error("rest.HandleUpdateCatalogRecord: Error parsing request body")
			return
		}

		getOpts = append(getOpts, service.NewPaginationOption(request.From, request.Size))
	}

	recordList, err := s.catalog.GetCatalogRecordList(getOpts...)
	if err != nil {
		zap.S().With("err", err).Error("rest.HandlerListCatalogRecord: Error listing records")
		ctx.JSON(http.StatusInternalServerError, internalErrorResponse)
		return
	}

	responseList := make([]catalogRecordModel, 0, len(recordList))
	for _, record := range recordList {
		responseList = append(responseList, catalogRecordModel{
			ID:                 record.ID,
			ProductName:        record.ProductName,
			ProductDescription: record.ProductDescription,
			ProductPrice:       record.ProductPrice,
			Availability:       record.Availability.String(),
		})
	}

	ctx.JSON(http.StatusOK, responseList)
}

func (s *RestHttpServer) HandleGetCatalogRecord(ctx *gin.Context) {
	recordIdString := ctx.Param("id")

	recordId, err := strconv.ParseInt(recordIdString, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, badRequestResponse)
		return
	}

	record, err := s.catalog.GetCatalogRecord(uint(recordId))

	if errors.Is(err, service.ErrRecordNotExists) {
		ctx.JSON(http.StatusNotFound, recordNotFoundResponse)
		return
	} else if err != nil {
		zap.S().With("err", err).Error("rest.HandleGetCatalogRecord: Error getting record")
		ctx.JSON(http.StatusInternalServerError, internalErrorResponse)
		return
	}

	ctx.JSON(http.StatusOK, &catalogRecordModel{
		ProductName:        record.ProductName,
		ProductDescription: record.ProductDescription,
		ProductPrice:       record.ProductPrice,
		Availability:       record.Availability.String(),
	})
}
