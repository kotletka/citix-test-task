package rest

import (
	"citix-test-task/internal/cfg"
	"citix-test-task/internal/service"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
)

type RestHttpServer struct {
	appCtx  context.Context
	config  *cfg.AppConfig
	catalog *service.CatalogService
}

func NewRestHttpServer(appCtx context.Context, config *cfg.AppConfig, catalog *service.CatalogService) *RestHttpServer {
	return &RestHttpServer{
		appCtx:  appCtx,
		config:  config,
		catalog: catalog,
	}
}

func (s *RestHttpServer) ListenAndServe() error {
	r := gin.Default()
	s.setRoutes(r)

	httpServer := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", s.config.HTTP.ListenAddr, s.config.HTTP.ListenPort),
		Handler: r,
	}

	var ctxClosed bool
	errChan := make(chan error)
	defer close(errChan)

	go func() {
		err := httpServer.ListenAndServe()
		if !ctxClosed {
			errChan <- err
		}
	}()

	select {
	case <-s.appCtx.Done():
		ctxClosed = true
		_ = httpServer.Shutdown(context.Background())
		return nil
	case err := <-errChan:
		zap.S().With("err", err).Error("Error occured when serving http requests")
		return err
	}
}

func (s *RestHttpServer) setRoutes(r *gin.Engine) {
	r.POST("/catalog/create", s.HandleCreateCatalogRecord)
	r.DELETE("/catalog/delete/:id", s.HandleDeleteCatalogRecord)
	r.POST("/catalog/update/:id", s.HandleUpdateCatalogRecord)
	r.GET("/catalog/get/:id", s.HandleGetCatalogRecord)
	r.GET("/catalog/list", s.HandleListCatalogRecord)
	r.POST("/catalog/list/page", s.HandleListCatalogRecord)
}
