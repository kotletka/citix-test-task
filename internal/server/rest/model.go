package rest

var (
	badRequestResponse = map[string]interface{}{
		"error": "Bad request",
	}

	internalErrorResponse = map[string]interface{}{
		"error": "Internal server error",
	}

	recordNotFoundResponse = map[string]interface{}{
		"error": "Record not found",
	}
)

type catalogRecordModel struct {
	ID                 uint   `json:"id,omitempty"`
	ProductName        string `json:"productName,omitempty"`
	ProductDescription string `json:"productDescription,omitempty"`
	ProductPrice       uint64 `json:"productPrice,omitempty"`
	Availability       string `json:"availability,omitempty"`
}

type listPaginationParameters struct {
	From int `json:"from"`
	Size int `json:"size"`
}
