package model

type CatalogRecord struct {
	ID                 uint `gorm:"primaryKey;autoIncrement"`
	ProductName        string
	ProductDescription string
	ProductPrice       uint64
	Availability       uint8
}
