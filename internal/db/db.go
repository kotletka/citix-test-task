package db

import (
	"citix-test-task/internal/cfg"
	"citix-test-task/internal/db/model"
	"fmt"
	"github.com/pkg/errors"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func InitializeDB(config *cfg.AppConfig) (*gorm.DB, error) {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%d sslmode=disable",
		config.DB.Host,
		config.DB.Username,
		config.DB.Password,
		config.DB.DatabaseName,
		config.DB.DatabasePort,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, errors.Wrap(err, "db.InitializeDB")
	}

	_ = db.AutoMigrate(model.CatalogRecord{})
	return db, nil
}
